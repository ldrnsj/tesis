import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AutenticacionService } from "../_services/autenticacion.service";
import { ProcesosService } from "../_services/procesos.service";
import { environment } from '../../environments/environment';


import sdk from '@stackblitz/sdk';
import { Leccion } from "../_interfaces/leccion.interface";

@Component({
  selector: 'app-html',
  templateUrl: './html.component.html',
  styleUrls: ['./html.component.css']
})

export class HtmlComponent implements OnInit {
  user; parrafo; br; input; a; button: string;
  headers: string;
  blitz: string = '"https://stackblitz.com/edit/angular-hr5jbh?embed=1&file=src/app/app.component.html"';


  constructor(private router: Router, private auth: AutenticacionService, private procesos: ProcesosService) { }


  values: string = '';
  et: string[];
  etCss: string[];
  model: any = {};
  loading = false;
  error: string;
  userP: string;
  datos: any;
  html:boolean;
  mostrarB: boolean=false;
  mostrarE: boolean=false;
  mostrarT: boolean=false;
  mostrarF: boolean=false;

  onKey(event: any) { // without type info
    //this.values += event.target.value + ' | ';
    this.values = event.target.value;
    console.log("Profesor seleccionado:" + this.values);
    
  }
 

  submit() {
    console.log("Haciendo submit del profesor: " + this.values);
    console.log("Register was Clicked");
    this.userP = sessionStorage.getItem('usuario');
    this.procesos.consultarCI(this.userP).subscribe(
      res => {
        this.datos = res[0].ci;
        console.log("Datos User " + this.datos + " RES " + res);
        console.log("JSON de DB RESPONSE: " + JSON.stringify(res));
        /*  $(document).ready(() =>{this.initTable();}); */
        var registerData: Leccion = {
          ci: this.datos,
          leccion: "html",
          profe: this.values

        };
        console.log("variables del Register: ci " + registerData.ci + " leccion  " + registerData.leccion + " profe: " + registerData.profe);
        this.crearLeccion(registerData);

      });
    console.log("antes del registerdata" + this.datos);



    console.log("despues del registerdata");


  }

  crearLeccion(registerData) {

    this.procesos.register(registerData).subscribe(
      success => {
        //this.router.navigate(['/login']);
        alert("Curso guardado con exito");
        location.reload();

      },
      error => {
        this.error = "Error: Verifique datos" + error;
        // TODO: Quitar para debug

      }
    )
  }

  ngOnInit() {
    this.html=true;     
    this.user = "test";  
    this.et = ["<!DOCTYPE html>", "<html>", "<head>", "<title>", "<body>", "<h1>", "<p>", "</p>", "</tittle>", "</head>", "</h1>", "</p1>", "</body>", "</html>", "<h6>", "<a>", "</a>", "<img>", "<button>", "</button>", "<ul>",
      "</ul>", "<li>", "</li>", "<ol>", "<br>", "<video>", "</video>", "<b>", "<strong>", "<i>", "<em>", "<mark>", "<small>", "<del>", "<ins>", "<sub>", "<sup>","<table>","<tr>","<th>","<td>","</table>","</tr>","</th>","</td>",
      "<form>","</form>", "<input>","<input type='text'>","<input type='radio'>","<input type='submit'>" ];
  
      this.userP = sessionStorage.getItem('usuario');
      this.procesos.getProfe().subscribe(
        res => {
          this.datos = res;
          console.log("Datos User " + this.datos + " RES " + res);
          console.log("JSON de DB RESPONSE: " + JSON.stringify(res));
        });
    this.etCss=["<p>"];
  
    }

}

const HelloWorld = {
  files: {
    'index.html': "<!DOCTYPE html> <html><body><h1>Primer Header</h1><p>Primer parrafo.</p></body></html>",
    'index.ts': `// I'm empty.`,
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};
const basicTags = {
  files: {
    'index.html': "<h1>Este es el título 1</h1> <h2>Este es el título 2</h2> <h3>Este es el título 3</h3> <p>Este es un parrafo.</p> <a href='https://www.google.com'>Este es el link</a>   <br>         <img src='https://bulldogfrancesorizaba.files.wordpress.com/2015/11/bull2.jpg' alt='Buldog Frnaces' width='154' height='162'>   <br> <video src='https://s3.amazonaws.com/codecademy-content/courses/freelance-1/unit-1/lesson-2/htmlcss1-vid_brown-bear.mp4' width='320' height='240' controls>Video no soportado</video> <br>   <button>Tocame</button> <br> Lista de razas de perros <ul>  <li>Buldog Frances</li>   <li>Golden Retriver</li>  <li>Dalmata</li> <ul>",
    'index.ts': `// I'm empty.`,
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};
const tabla = {
  files: {
    'index.html': "<table> <tr> <th>Nombre</th> <th>Apellido</th> <th>Edad</th> </tr> <tr> <td>Alejandra</td> <td>Becerra</td>  <td>24</td> </tr> <tr> <td>Jose</td> <td>Cabeza</td> <td>24</td> </tr> </table>",
    'index.ts': `// I'm empty.`,
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};
const formulario = {
  files: {
    'index.html': "<form> Nombre: <br> <input type='text' name='nombre'><br> Last name:<br> <input type='text' name='apellido'> <br><input type='radio' name='gender' value='masculino'checked> Masculino<br> <input type='radio' name='gender' value='femenino'> Femenino<br> <input type='radio' name='gender' value='otro'> Otro<br> <input type='submit' value='Enviar'>           </form>",
    'index.ts': `// I'm empty.`,
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};



window['hello'] = () => {
  sdk.embedProject('hello', HelloWorld, {
    openFile: 'index.html'
  })
    // Get the VM of the embedded instance
    .then(vm => {
      // Wait 2 seconds...
      setTimeout(() => {
        // Then update the VM's file system :)
        /* vm.applyFsDiff({
          create: {
            'index.html': `<h1>This was updated programmatically!</h1>`
          },
          destroy: ['randomFile.ts']
        }); */
      }, 1000)
    });
}
window['tags'] = () => {
  sdk.embedProject('tags', basicTags, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

window['table'] = () => {
  sdk.embedProject('table', tabla, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

window['formu'] = () => {
  sdk.embedProject('formu', formulario, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

