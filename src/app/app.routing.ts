import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { NgModule } from "@angular/core";
import { AutenticacionGuard } from "./_services";

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CarrouselComponent } from './carrousel/carrousel.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UserComponent } from './user/user.component';
import { NavbarHTMLComponent } from './navbar-html/navbar-html.component';
import { ContactComponent } from './contact/contact.component';
import { HtmlComponent } from './html/html.component';
import { CssComponent } from './css/css.component';
import { JsComponent } from './js/js.component';
import { PhpComponent } from './php/php.component';


import { TestComponent } from './test/test.component';
import { AngularComponent } from './angular/angular.component';
import { FooterComponent } from './footer/footer.component';



const appRoutes: Routes = [
    {path:'home' , component : HomeComponent,canActivate:[AutenticacionGuard]},
    {path:'navbarHTML' , component : NavbarHTMLComponent},
    {path:'navbar' , component : NavbarComponent},
    {path:'carrousel' , component : CarrouselComponent},
    {path:'about' , component : AboutComponent,canActivate:[AutenticacionGuard]},
    {path:'contact' , component : ContactComponent,canActivate:[AutenticacionGuard]},
    {path:'html' , component:HtmlComponent,canActivate:[AutenticacionGuard]},
    {path:'css' , component:CssComponent,canActivate:[AutenticacionGuard]},
    {path:'js' , component:JsComponent,canActivate:[AutenticacionGuard]},
    {path:'php' , component:PhpComponent,canActivate:[AutenticacionGuard]},
    {path: 'test', component:TestComponent},

    {path:'user' , component:UserComponent,canActivate:[AutenticacionGuard]},

    {path:'footer',component:FooterComponent},

    {path:'' , component : LoginComponent, pathMatch:'full'},
    {path:'**', redirectTo:''}, //pilas
  
/*     { path: '', redirectTo: '/bandeja', pathMatch: 'full' },
 */    { path: 'redirectTologin', redirectTo: '/login', pathMatch: 'full' },
    { path: '**', redirectTo: '/404' } // No encontrado
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes,{useHash: true})
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AutenticacionGuard
  ]

})
export class AppRoutingModule {}
