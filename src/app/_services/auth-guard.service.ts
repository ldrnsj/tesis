import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";

@Injectable()
export class AutenticacionGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate() {
    if (sessionStorage.getItem('usuario')) {
      // Logeado retonar true
      return true;
    }

    // No esta logeado redireccionar a la pagina de login
    this.router.navigate(['/login']);
    return false;
  }
}
