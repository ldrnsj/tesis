import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/toPromise';
import { map, catchError } from 'rxjs/operators';
import {Observable,of, from } from 'rxjs';
//import 'rxjs/add/operator/toPromise';
import { environment } from "../../environments/environment";
import { Login } from "../_interfaces/login.interface";
import { Register } from "../_interfaces/register.interface";
import {BehaviorSubject} from "rxjs";

//import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { reject } from 'q';
//import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
//import { map, filter, switchMap  } from 'rxjs/operators';
//import {ProcesosService} from '../_services/procesos.service'; 

@Injectable()
export class AutenticacionService {
  private httpOptions: RequestOptions; // Opciones necesarios para mantener la conexion
  public username; // Puede sustituirse por un token en el futuro

  constructor(private router: Router, private http: Http) {
    let headers = new Headers({});
    this.httpOptions = new RequestOptions({ headers: headers, withCredentials: true });
    this.router.events.subscribe(
      result => {
       // if (result.url == "/login") {
         // this.logout();
       // }
      });
  }
  login(loginData: Login): Observable<any> {
    console.log(this.http+environment.potra);
    
    return this.http.get(environment.potra+'/Login'+'?nickname='+loginData.nickname+'&passwords='+loginData.passwords,this.httpOptions)
      .pipe(map((res: Response) => {
        console.log("RESPONSE WS: "+res);
        if (res.status == 200 && JSON.stringify(res.json())!='[]') {
          this.username = loginData.nickname;
          sessionStorage.setItem('usuario', loginData.nickname);// Guardamos sesion
          sessionStorage.setItem('Key', "true");
          console.log("JSON de DB RESPONSE: "+JSON.stringify(res.json()));    
          return res.status; 
        }
        else{
          console.log("Usuario sin match"); 
          sessionStorage.setItem('Key', "false");

          Response => Observable.throw('Server error');
          throw(res.status);}        
      }))
      .pipe(catchError((error: Response) => Observable.throw(error || 'Server error')));
  }

  register(registerData:Register): Observable<any>{
    console.log("Estamos INTENTANDO REGISTRAR");    
    return this.http.post(environment.potra+'/Register'+'?nickname='+registerData.nickname+'&ci='+registerData.ci+'&passwords='+registerData.password+'&nombre='+registerData.nombre+'&apellido='+registerData.apellido+'&email='+registerData.email,this.httpOptions)
    .pipe(map((res: Response) => {
      console.log("RESPONSE WS: "+res);
      if (res.status == 200) {
        this.username = registerData.nickname;
        sessionStorage.setItem('usuario', registerData.nickname);// Guardamos sesion
        console.log("JSON de DB RESPONSE: "+JSON.stringify(res.json()));    
        sessionStorage.setItem('Key', "true");

        return res.status;      
      }else{
        console.log("Usuario sin match"); 
        sessionStorage.setItem('Key', "false");
 
        throw(res.status);}      
    }))
    .pipe(catchError((error: Response) => Observable.throw(error || 'Server error')));
}  
 logout() {
    sessionStorage.removeItem('usuario'); // Eliminamos el usuario de local Storage
    this.loggedIn.next(false);
    console.log("logout");
    
    this.router.navigate(['']);
  }

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    var setted:boolean=false; 
    if(sessionStorage.getItem('usuario')!=null)
    {
      setted=true;
      console.log("Tenemos un usuario: "+sessionStorage.getItem('usuario'));

    }
    return setted;
  }

}
