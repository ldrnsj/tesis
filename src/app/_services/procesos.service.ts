import { RequestOptions, Headers, Http, Response,ResponseContentType } from "@angular/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import * as FileSaver from 'file-saver';
import {RegisterLeccion}from "../_interfaces/registerLeccion.interface";
import { map, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";



@Injectable()
export class ProcesosService {
  private httpOptions: RequestOptions; // Opciones necesarios para mantener la conexion
  

  private withCredentials: RequestOptions; // Opcion para mantener las cookies devueltas por el ESB
  constructor(private router: Router, private http: Http) {
    let headers = new Headers({});
    this.withCredentials = new RequestOptions({ headers: headers, withCredentials: true});
    this.httpOptions = new RequestOptions({ headers: headers, withCredentials: true });
    this.router.events.subscribe(
      result => {
       // if (result.url == "/login") {
         // this.logout();
       // }
      });
    

  }

  

  closeSes(error: Response) {
    // Se vencio el token de autenticacion, regresamos al login
    if (error.status == 401) {
      sessionStorage.clear();
      alert("Su sesion expiro, ingrese nuevamente");
    }
    return Observable.throw(error || 'Server error')
  }


  getProfe():Observable<any>{    
    let url='/profeList';
    url=environment.potra+ url;    
    console.log("DATA: " +this.http.get(url).pipe(map(this.extractData)));    
    return this.http.get(url).pipe(map(this.extractData)) ;  
  }



  getLeccion(user:string):Observable<any>{
    
    let url='/check'+'?nickname='+user;
    url=environment.potra+ url;

    console.log("Procesos usuario"+user+ " y URL : "+url );
    console.log(this.http.get(url).pipe(map(this.extractData)));
    
    return this.http.get(url).pipe(map(this.extractData)) ;  
  }

  verifyType(user:string):Observable<any>{
    
    let url='/type'+'?nickname='+user;
    url=environment.potra+ url;

    console.log("Procesos usuario"+user+ " y URL : "+url  );
    console.log("CI del profesor "+this.http.get(url).pipe(map(this.extractData)));
    
    return this.http.get(url).pipe(map(this.extractData)) ;  
  }

  consultarCI(nickname:string):Observable<any>{
    let url='/usuarios'+'?nickname='+nickname;
    url=environment.potra+ url;

    console.log("Procesos usuario"+nickname+ " y URL : "+url );
    console.log(this.http.get(url).pipe(map(this.extractData)));
    
    return this.http.get(url).pipe(map(this.extractData)) ;  

  }



  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  register(registerData:RegisterLeccion): Observable<any>{
    console.log("Estamos INTENTANDO REGISTRAR LA LECCION");    
    return this.http.post(environment.potra+'/leccion'+'?ci='+registerData.ci+'&profe='+registerData.profe+'&leccion='+registerData.leccion,this.httpOptions)
    .pipe(map((res: Response) => {
      console.log("RESPONSE WS: "+res);
      if (res.status == 200) {
        console.log("JSON de DB RESPONSE: "+JSON.stringify(res.json()));    

        return res.status;      
      }else{
        console.log("Error al ingresar");  
        throw(res.status);}      
    }))
    .pipe(catchError((error: Response) => Observable.throw(error || 'Server error')));
}  

}
