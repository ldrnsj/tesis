import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarHTMLComponent } from './navbar-html.component';

describe('NavbarHTMLComponent', () => {
  let component: NavbarHTMLComponent;
  let fixture: ComponentFixture<NavbarHTMLComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarHTMLComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarHTMLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
