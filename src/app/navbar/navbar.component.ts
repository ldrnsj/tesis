import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from "../_services/autenticacion.service";
import { Router } from "@angular/router";
import {Observable} from "rxjs";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn$: boolean;
  constructor(private router: Router, private auth: AutenticacionService) {
  }
  
  ngOnInit() {
    console.log("NAV BAR DICE "+sessionStorage.getItem('usuario'));
   this.Checker();

   
   if (document.readyState){
     console.log("ENTRO AL STATE");
     
     
    
    // grab the initial top offset of the navigationss
       var stickyNavTop = document.getElementById('test').offsetTop;
       
       // our function that decides weather the navigation bar should have "fixed" css position or not.
       var stickyNav = function(){
        var scrollTop = window.scrollY; // our current vertical position from the top
             
        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative
        if (scrollTop > stickyNavTop) { 
          document.getElementById('test').classList.add('sticky');
        } else {
          document.getElementById('test').classList.remove('sticky'); 
        }
    };// END FUNCTION STICKYNAV

    stickyNav();
    // and run it again every time you scroll
    window.onscroll = function() {
      stickyNav();
    };
  }


     
    }
    onLogout() {

      this.auth.logout();
      //location.reload();

    }
    Checker(){ 
      if(this.auth.isLoggedIn){
        this.isLoggedIn$=true;
      }
      else{
        this.isLoggedIn$=false;
      }
    }



}

