import { Component, OnInit } from '@angular/core';
import { ProcesosService } from '../_services'
import { Router } from "@angular/router";
import { ValidateCourse} from '../_interfaces/validateCourse.interface';

/* import DataTable=DataTables.DataTable;
 */@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  datos:any;
  public tabla:any;
  userP:string;
  count: number = 0;
  buttonDisabled: boolean = false;
  model: any = {};
  ci:any;
  html:boolean=false;
  css:boolean=false;
  js:boolean=false;
  php:boolean=false;
  isAlumno:boolean=true;
  t_user:any;


  
  constructor(private router: Router,private procesos:ProcesosService) { 

  }

  ngOnInit() {
    this.userP=sessionStorage.getItem('usuario');
    console.log("Comienza verificacion");
    
    this.procesos.verifyType(this.userP).subscribe(
      cod_user=>{
        console.log("Verificando");
        
      this.t_user=cod_user[0];
      console.log("Resul "+this.t_user+" truncado "+this.t_user.tipo);
      
      if (this.t_user.tipo==1){this.isAlumno=false;}
      });
    this.procesos.getLeccion(this.userP).subscribe(
      res=>{
      this.datos=res;
      console.log("Datos User "+this.datos[0]+  " RES "+ res  );
      console.log("JSON de DB RESPONSE: "+JSON.stringify(res));  
        
      console.log("ANTES DEL FOR");
      console.log(this.datos);
      let result=this.procesos.consultarCI(this.userP).subscribe(
        fin => {this.ci = fin[0].ci;
        
          console.log("fin" +result+ this.ci);
          for (let index = 0; index < this.datos.length; index++) {
            const element = this.datos[index];
            console.log('element var '+element);
            if (element.leccion=='html' && element.ci== this.ci)
                
                {

                this.html=true;
                console.log("MEDALLA HTML");
            }
            else {if(element.leccion=='css' && element.ci== this.ci){
                this.css=true;
                console.log("MEDALLA CSS");
            }

            else {if (element.leccion=='js' && element.ci== this.ci){
                this.js=true;
                console.log("MEDALLA JS");

            }

            else {if (element.leccion=='php' && element.ci== this.ci){
              this.php=true;
              console.log("MEDALLA PHP");

              console.log("ROOKIE PROGRAMMER");
            }
          }}

            }  
          }
        }
        );
      });
    } 


 
  
  


    
  }

  


