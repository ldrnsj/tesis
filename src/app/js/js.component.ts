import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AutenticacionService } from "../_services/autenticacion.service";
import { ProcesosService } from "../_services/procesos.service";
import { environment } from '../../environments/environment';
import sdk from '@stackblitz/sdk';
import { Leccion } from "../_interfaces/leccion.interface";

@Component({
  selector: 'app-js',
  templateUrl: './js.component.html',
  styleUrls: ['./js.component.css']
})
export class JsComponent implements OnInit {

  
  user; parrafo; br; input; a; button: string;
  headers: string;
  blitz: string = '"https://stackblitz.com/edit/angular-hr5jbh?embed=1&file=src/app/app.component.html"';


  constructor(private router: Router, private auth: AutenticacionService, private procesos: ProcesosService) { }


  values: string = '';
  et: string[];
  etJs: string[];
  model: any = {};
  loading = false;
  error: string;
  userP: string;
  datos: any;
  html:boolean;
  mostrarB: boolean=false;
  mostrarB1: boolean=false;


  onKey(event: any) { // without type info
    //this.values += event.target.value + ' | ';
    this.values = event.target.value;
    console.log("Profesor seleccionado:" + this.values);
    
  }
 

  submit() {
    console.log("Haciendo submit del profesor: " + this.values);
    console.log("Register was Clicked");
    this.userP = sessionStorage.getItem('usuario');
    this.procesos.consultarCI(this.userP).subscribe(
      res => {
        this.datos = res[0].ci;
        console.log("Datos User " + this.datos + " RES " + res);
        console.log("JSON de DB RESPONSE: " + JSON.stringify(res));
        /*  $(document).ready(() =>{this.initTable();}); */
        var registerData: Leccion = {
          ci: this.datos,
          leccion: "js",
          profe: this.values

        };
        console.log("variables del Register: ci " + registerData.ci + " leccion  " + registerData.leccion + " profe: " + registerData.profe);
        this.crearLeccion(registerData);

      });
    console.log("antes del registerdata" + this.datos);



    console.log("despues del registerdata");

  }

  crearLeccion(registerData) {

    this.procesos.register(registerData).subscribe(
      success => {
        //this.router.navigate(['/login']);
        alert("Curso guardado con exito");
        location.reload();

      },
      error => {
        this.error = "Error: Verifique datos" + error;
        // TODO: Quitar para debug

      }
    )
  }
  ngOnInit() {
    this.html=true;     
    this.user = "test";  
      this.userP = sessionStorage.getItem('usuario');
      this.procesos.getProfe().subscribe(
        res => {
          this.datos = res;
          console.log("Datos User " + this.datos + " RES " + res);
          console.log("JSON de DB RESPONSE: " + JSON.stringify(res));
        });
    this.etJs=["<img>","<", "{", "}"];
  }



}
