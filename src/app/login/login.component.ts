import { Component, OnInit } from '@angular/core';
import * as Animacion from '../../assets/js/animation.js';
import { environment } from '../../environments/environment';
import { Router } from "@angular/router";
import { AutenticacionService } from "../_services/autenticacion.service";
import { Login } from "../_interfaces/login.interface";
import { Register } from "../_interfaces/register.interface";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  jquery: any;
  change: true;

  model: any = {};
  loading = false;
  error: string;

  constructor(private router: Router, private auth: AutenticacionService) {
  }
  ngOnInit() {
  }

  submit() {
    console.log("Submit was Clicked");   
    var loginData: Login = {
      nickname: this.model.username,
      passwords: this.model.password,
    };
    console.log("variables del login: "+loginData.nickname+ " and  "+loginData.passwords);
    this.auth.login(loginData).subscribe(
      success => { 
        console.log("Login exitoso: " + success +" Usuario: "+sessionStorage.getItem('usuario'));
        //$route.reload();
        alert("Usuario logeado con exito");
        this.router.navigate(['/home']);
        
        //location.reload();
      },
      error => {
        console.log("Entro en error de login");
        //alert("ERROR log");

        
        this.error = "Error: Verifique su usuario y contraseña";
        // TODO: Quitar para debug
        if (environment.json) {
          sessionStorage.setItem('usuario', loginData.nickname);
          this.router.navigate(['/login']);
        }
      }
    )
  }

  Regis() {
    console.log("Register was Clicked");   
    var registerData: Register = {
      nickname: this.model.nickname,
      ci: this.model.ci,
      password: this.model.pass,
      email: this.model.email,
      nombre: this.model.name,
      apellido: this.model.l_name
    };
    console.log("variables del Register: "+registerData.nickname+ " and  "+registerData.password+ " ci: "+registerData.ci);
    this.auth.register(registerData).subscribe(
      success => { 
        console.log("Register exitoso: " + success +" Usuario nuevo: "+sessionStorage.getItem('usuario'));
        //this.router.navigate(['/login']);
        alert("Usuario registrado con exito");
        location.reload();
        
      },
      error => {
        this.error = "Error: Verifique datos" + error;
        // TODO: Quitar para debug
        if (environment.json) {
          sessionStorage.setItem('usuario', registerData.nickname);
          this.router.navigate(['/login']);
        }
      }
    )
}





}
