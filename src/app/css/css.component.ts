import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AutenticacionService } from "../_services/autenticacion.service";
import { ProcesosService } from "../_services/procesos.service";
import { environment } from '../../environments/environment';


import sdk from '@stackblitz/sdk';
import { Leccion } from "../_interfaces/leccion.interface";
@Component({
  selector: 'app-css',
  templateUrl: './css.component.html',
  styleUrls: ['./css.component.css']
})
export class CssComponent implements OnInit {

  user; parrafo; br; input; a; button: string;
  headers: string;
  blitz: string = '"https://stackblitz.com/edit/angular-hr5jbh?embed=1&file=src/app/app.component.html"';


  constructor(private router: Router, private auth: AutenticacionService, private procesos: ProcesosService) { }


  values: string = '';
  et: string[];
  etCss: string[];
  model: any = {};
  loading = false;
  error: string;
  userP: string;
  datos: any;
  html:boolean;
  mostrarT: boolean=false;
  mostrarF: boolean=false;
  mostrarF2: boolean=false;
  mostrarEI: boolean=false;
  mostrarEL: boolean=false;
  mostrarEE: boolean=false;
  mostrarE: boolean=false;


  onKey(event: any) { // without type info
    //this.values += event.target.value + ' | ';
    this.values = event.target.value;
    console.log("Profesor seleccionado:" + this.values);
    
  }
 

  submit() {
    console.log("Haciendo submit del profesor: " + this.values);
    console.log("Register was Clicked");
    this.userP = sessionStorage.getItem('usuario');
    this.procesos.consultarCI(this.userP).subscribe(
      res => {
        this.datos = res[0].ci;
        console.log("Datos User " + this.datos + " RES " + res);
        console.log("JSON de DB RESPONSE: " + JSON.stringify(res));
        /*  $(document).ready(() =>{this.initTable();}); */
        var registerData: Leccion = {
          ci: this.datos,
          leccion: "css",
          profe: this.values

        };
        console.log("variables del Register: ci " + registerData.ci + " leccion  " + registerData.leccion + " profe: " + registerData.profe);
        this.crearLeccion(registerData);

      });
    console.log("antes del registerdata" + this.datos);



    console.log("despues del registerdata");


  }

  crearLeccion(registerData) {

    this.procesos.register(registerData).subscribe(
      success => {
        //this.router.navigate(['/login']);
        alert("Curso guardado con exito");
        location.reload();

      },
      error => {
        this.error = "Error: Verifique datos" + error;
        // TODO: Quitar para debug

      }
    )
  }
  ngOnInit() {
    this.html=true;     
    this.user = "test";  
      this.userP = sessionStorage.getItem('usuario');
      this.procesos.getProfe().subscribe(
        res => {
          this.datos = res;
          console.log("Datos User " + this.datos + " RES " + res);
          console.log("JSON de DB RESPONSE: " + JSON.stringify(res));
        });
    this.etCss=["<p>","<link>","<head>","</head>","<link  rel='stylesheet' type='text/css' href='mystyle.css'>",
     "<style>","</style>","<h1 style='color:blue;margin-left:30px;'>","{","}", "<th>","<td>"];

  }


}

const basicCss = {
  files: {
    'index.html': "<h1>Este es el título 1</h1> <h2 id='ejemplo'>Este es el título 2</h2> <h3 class='center'>Este es el título 3</h3>",
    'index.css': "h1 {text-align: center; color: red;} #ejemplo {text-align: center; color: blue;} .center {text-align: center;color: yellow;}",
    'randomFile.ts': '// You should delete me.',
    '.angular-cli.json':'{"apps": [{"styles": ["index.css"]}]}'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};


const styleExterno = {
  files: {
    'index.html': "<html> <head> <link rel='stylesheet' type='text/css' href='mystyle.css'> </head> <body> <h1>Este es un título</h1> <p>Este es un párrafo.</p> </body>  </html>",
    'index.css': "body {background-color: black;} h1 {color: cyan; margin-left: 20px; } p{color: white}",
    'randomFile.ts': '// You should delete me.',
    '.angular-cli.json':'{"apps": [{"styles": ["index.css"]}]}'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};

const styleInterno = {
  files: {
    'index.html': "<html> <head> <style> body { background-color: linen;}h1 { color: maroon;margin-left: 40px;} </style> </head> <body> <h1>Este es un título</h1> <p>Este es un párrafo.</p> </body>  </html>",
    'randomFile.ts': '// You should delete me.',
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};

const styleLinea = {
  files: {
    'index.html': "<html><body> <h1 style='color:blue;margin-left:30px;'>Este es un título</h1> <p>Este es un párrafo.</p> </body>  </html>",
    'randomFile.ts': '// You should delete me.',
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};

const backgroundColor = {
  files: {
    'index.html': "<html><head><style>body {background-color: lightblue;}</style> </head> <body><h1>Hey!</h1><p>Esta página tiene un color de fondo</p></body> </html>",
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};

const backgroundImg = {
  files: {
    'index.html': "<html><head><style>body {  background-image: url('css-example.png'); background-repeat: no-repeat; background-position: right top; margin-right: 200px; background-attachment: fixed;}</style> </head> <body><h1>Hey!</h1><p>La imagen de fondo es fija. Y esta hacia la derecha</p></body> </html>",
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};

const table = {
  files: {
    'index.html': "<html><head><style>table {border-collapse: collapse;border: 1px solid black; width: 100%;} th, td {text-align: left;padding: 8px;border: 1px solid black;border-bottom: 1px solid #ddd;}   tr:nth-child(even){background-color: #f2f2f2} tr:hover {background-color: #2DE5E5;}th {background-color: #24B7B7;color: white;}td{vertical-align: bottom; height: 25px;} </style> <body> <table><tr><th>Nombre</th><th>Apellido</th><th>Ahorros</th></tr><tr><td>Jose</td><td>Cabeza</td><td>100 BsS</td></tr><tr><td>Alejandra</td><td>Becerra</td><td>1000 BsS</td></tr><tr><td>Juan</td><td>Martinez</td><td>300 BsS</td></tr><tr><td>Alejandro</td><td>Serrano</td><td>950 BsS</td></tr></table></body></head></html>",
    'randomFile.ts': '// You should delete me.'
  },
  title: 'Prueba de pagina',
  description: 'Creado para uso interno',
  template: 'typescript',
  tags: ['stackblitz', 'sdk']
};

window['basic'] = () => {
  sdk.embedProject('basic', basicCss, {
    openFile: 'index.css'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

window['estiloExterno'] = () => {
  sdk.embedProject('estiloExterno', styleExterno, {
    openFile: 'index.css'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}
window['estiloInterno'] = () => {
  sdk.embedProject('estiloInterno', styleInterno, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

window['estiloLinea'] = () => {
  sdk.embedProject('estiloLinea', styleLinea, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

window['backgroundColor'] = () => {
  sdk.embedProject('backgroundColor', backgroundColor, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}


window['backgroundImg'] = () => {
  sdk.embedProject('backgroundImg', backgroundImg, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}

window['table'] = () => {
  sdk.embedProject('table', table, {
    openFile: 'index.html'
  })
    .then(vm => {
      setTimeout(() => { }, 1000)
    });
}