import {RouterModule, Routes} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {bootstrap} from 'bootstrap';
import {DataTableModule} from "angular-6-datatable";

import { AppComponent } from './app.component';
import { AppRoutingModule } from "./app.routing";
import { NavbarComponent } from './navbar/navbar.component';
import { CarrouselComponent } from './carrousel/carrousel.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { TestComponent } from './test/test.component';
import { AngularComponent } from './angular/angular.component';


import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { ProcesosService, AutenticacionService } from "./_services";
import { SecurityComponent } from './security/security.component';
import { HtmlComponent } from './html/html.component';
import { FooterComponent } from './footer/footer.component';
import { UserComponent } from './user/user.component';
import { NavbarHTMLComponent } from './navbar-html/navbar-html.component';
import { CssComponent } from './css/css.component';
import { PhpComponent } from './php/php.component';
import { JsComponent } from './js/js.component';






@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CarrouselComponent,
    LoginComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    TestComponent,
    AngularComponent,
    SecurityComponent,
    HtmlComponent,
    FooterComponent,
    UserComponent,
    NavbarHTMLComponent,
    CssComponent,
    PhpComponent,
    JsComponent  
  ],
  imports: [
    //RouterModule.forRoot(rutasApp),
   // BrowserModule
    BrowserModule,
    DataTableModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot(),

  ],
  providers: [AutenticacionService,
    ProcesosService,
    { provide: LOCALE_ID, useValue: "es-VE" }],
  bootstrap: [AppComponent]
})
export class AppModule { }
